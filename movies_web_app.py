import os
import time

from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode


application = Flask(__name__)
app = application


def get_db_creds():
    db = os.environ.get("DB", None) or os.environ.get("database", None)
    username = os.environ.get("USER", None) or os.environ.get("username", None)
    password = os.environ.get("PASSWORD", None) or os.environ.get("password", None)
    hostname = os.environ.get("HOST", None) or os.environ.get("dbhost", None)
    return db, username, password, hostname


def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
    db, username, password, hostname = get_db_creds()
    table_ddl = 'CREATE TABLE movies(id INT UNSIGNED NOT NULL AUTO_INCREMENT, year YEAR(4), title VARCHAR(255), director VARCHAR(255), actor VARCHAR(255), release_date DATE, rating INT UNSIGNED, PRIMARY KEY (id))'

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        #try:
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
        #except Exception as exp1:
        #    print(exp1)

    cur = cnx.cursor()

    try:
        cur.execute(table_ddl)
        cnx.commit()
        populate_data()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)


def webpage(response):
    if type(response) != list:
        return render_template('index.html', message=[dict(message=response)])
    return render_template('index.html', message=response)


@app.route('/add_movie', methods=['POST'])
def add_movie():
    print("Received request.")

    year = request.form['year']
    title = request.form['title']
    director = request.form['director']
    actor = request.form['actor']
    release_date = ''
    try:
        release_date = time.strftime('%Y-%m-%d', time.strptime(request.form['release_date'], '%d %b %Y'))
    except Exception as exp:
        return webpage('Invalid Date Format - Should be Day(DD) Month(Name) Year(YYYY)')
    rating = request.form['rating']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try:
        cur = cnx.cursor()
        sql = 'INSERT INTO movies (year, title, director, actor, release_date, rating) VALUES (%s, %s, %s, %s, %s, %s)'
        cur.execute(sql,  (year, title.lower(), director.lower(), actor.lower(), release_date, rating))
        cnx.commit()
        return webpage('Movie ' + title + ' successfully inserted')
    except Exception as exp:
        return webpage('Movie ' + title + ' could not be inserted - ' + str(exp))
    finally:
        cur.close()


@app.route('/update_movie', methods=['POST'])
def update_movie():
    print("Received request.")

    year = request.form['year']
    title = request.form['title']
    director = request.form['director']
    actor = request.form['actor']
    release_date = ''
    try:
        release_date = time.strftime('%Y-%m-%d', time.strptime(request.form['release_date'], '%d %b %Y'))
    except Exception as exp:
        return webpage('Invalid Date Format - Should be Day(DD) Month(Name) Year(YYYY)')
    rating = request.form['rating']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try:
        cur = cnx.cursor()
        sql = 'UPDATE movies SET year=%s, director=%s, actor=%s, release_date=%s, rating=%s WHERE title=%s'
        cur.execute(sql, (year, director.lower(), actor.lower(), release_date, rating, title.lower()))
        cnx.commit()

        if cur.rowcount:
            return webpage('Movie ' + title + ' successfully updated')
        return webpage('Movie ' + title + ' could not be updated - No move with the title ' + title)
    except Exception as exp:
        return webpage('Movie ' + title + ' could not be updated - ' + str(exp))
    finally:
        cur.close()


@app.route('/delete_movie', methods=['POST'])
def delete_movie():
    print("Received request.")

    title = request.form['delete_title']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try:
        cur = cnx.cursor()
        sql = "DELETE FROM movies WHERE title='" + title.lower() + "'"
        cur.execute(sql)
        cnx.commit()
        return webpage('Movie ' + title + ' successfully deleted')
    except Exception as exp:
        return webpage('Movie ' + title + ' could not be deleted - ' + str(exp))
    finally:
        cur.close()


@app.route('/search_movie', methods=['POST'])
def search_movie():
    print("Received request.")

    actor = request.form['search_actor']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try:
        cur = cnx.cursor()
        sql = "SELECT title, year, actor FROM movies WHERE actor='" + actor.lower() + "'"
        cur.execute(sql)

        result = list(cur)
        if result:
            result_list = []
            for r in result:
                r = map(str, r)
                result_list.append(dict(message=' '.join(r)))
            return webpage(result_list)
        return webpage('No movies found for actor ' + actor)
    except Exception as exp:
        return webpage('Movies for actor ' + actor + ' could not be found - ' + str(exp))
    finally:
        cnx.commit()
        cur.close()


@app.route('/highest_rating', methods=['GET'])
def highest_rating():
    print("Received request.")

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try:
        cur = cnx.cursor()
        sql = 'SELECT title, year, actor, director, rating FROM movies WHERE rating = (SELECT MAX(rating) as min_rating FROM movies)'
        cur.execute(sql)

        result = list(cur)
        if result:
            result_list = []
            for r in result:
                r = map(str, r)
                result_list.append(dict(message=' '.join(r)))
            return webpage(result_list)
        return webpage('No movies in table')
    except Exception as exp:
        return webpage('Could not get highest rated movie - ' + str(exp))
    finally:
        cnx.commit()
        cur.close()


@app.route('/lowest_rating', methods=['GET'])
def lowest_rating():
    print("Received request.")

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try:
        cur = cnx.cursor()
        sql = 'SELECT title, year, actor, director, rating FROM movies WHERE rating = (SELECT MIN(rating) as min_rating FROM movies)'
        cur.execute(sql)

        result = list(cur)
        if result:
            result_list = []
            for r in result:
                r = map(str, r)
                result_list.append(dict(message=' '.join(r)))
            return webpage(result_list)
        return webpage('No movies in table')
    except Exception as exp:
        return webpage('Could not get lowestest rated movie - ' + str(exp))
    finally:
        cnx.commit()
        cur.close()


@app.route("/")
def hello():
    print("Inside hello")
    print("Printing available environment variables")
    print(os.environ)
    print("Before displaying index.html")
    create_table()
    return render_template('index.html')


if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')
